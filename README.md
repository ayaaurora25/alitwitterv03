# AliTwitter App

## Description
This application only used for personal tweet. 

## Environment Setup
- ruby 2.6.5
- rails 6.0.2.1
- rspec-rails 4.0.0.beta3
- puma 4.1
- yarn 1.22.4
- node 12.16.1
- sqlite3 1.4

install all depedency using bundle in root of directory project.
```bash
bundle install
```

## Database migration
Please type the following to migrate database
```bash
rails db:migrate
rails db:seed
```

## Run Test
To run rspec
```bash
bundle exec rspec
```

## Run Instruction
To run web-app, please type
```bash
rails server
```

the following output will be shown
```bash
=> Booting Puma
=> Rails 6.0.2.2 application starting in development 
=> Run `rails server --help` for more startup options
Puma starting in single mode...
* Version 4.3.3 (ruby 2.6.3-p62), codename: Mysterious Traveller
* Min threads: 5, max threads: 5
* Environment: development
* Listening on tcp://127.0.0.1:3000
* Listening on tcp://[::1]:3000
Use Ctrl-C to stop
```

on your browser url, please type http://127.0.0.1:3000/tweets/new

## Run Using Docker
### Run in Docker
To be able to run the web-app, you need to go to this project root directory.

To run in docker, you need to create a docker image
```shell
docker build -t IMAGE_NAME .
```

Example:
```shell
docker build -t alitwitter .
```

After you successfully create the docker image, you can run the docker image.
```shell
docker run -p 3000:3000 --name CONTAINER_NAME IMAGE_NAME 
```

Example:
```shell
docker run -p 3000:3000 --name alitwitter alitwitter
```

Once you run the docker image, you can access the web-app by typing http://127.0.0.1:3000/tweets/new on your url browser.

### Stop the Docker Container
If you want to stop the current running docker container, please type
```shell
docker stop CONTAINER_NAME
```

Example:
```shell
docker stop alitwitter
```

### Save the Docker Image
To save the docker image with `.tar` extension, you need to type the following
```shell
docker save --output FILE_OUTPUT.tar IMAGE_NAME
```

Example:
```shell
docker save --output alitwitter.tar alitwitter
```

## Run in Kubernetes
Before running the app, you need to install:
- [Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [Minikube](https://kubernetes.io/docs/setup/learning-environment/minikube/)
- [Virtualbox](https://www.virtualbox.org/wiki/Downloads)

in your machine.

After that, you need to start your `minikube` is running by typing
```
minikube start --driver=virtualbox
```

Before setting the deployment object, you need to set the configMap object by typing
```
kubectl apply -f k8s/config.yml
```

To set the deployment object, please run
```
kubectl apply -f k8s/deployment.yml
```

To set the service object, please run
```
kubectl apply -f k8s/service.yml
```

To access the app, please type
```
minikube service alitwitter-service --url
```

For example, the minikube URL result will be shown:
```
http://192.168.99.100:30399
```

Once you see the URL, you can access the app with the browser by typing `http://<minikube-url>/tweets/new`

To stop `minikube`
```
minikube stop
```

## Constraint
- only able to add new tweet, view tweets, and delete tweet within one page
