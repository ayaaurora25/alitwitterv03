require 'rails_helper'

describe Tweet, type: :model do
  describe 'validations tests' do
    context 'given text params is not present' do
      it 'return false' do
        tweet = Tweet.new
        expect(tweet.valid?).to eq(false)
      end
    end

    context 'given valid text params' do
      it 'should be able to save tweet' do
        tweet = Tweet.new(text: 'Test tweet')
        expect(tweet.save).to eq(true)
      end
    end
  end
end
