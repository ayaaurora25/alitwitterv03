require 'rails_helper'

RSpec.feature 'Tweets', type: :feature do
  context 'create new tweet' do
    before(:each) do
      visit new_tweet_path
      within('form') do
      end
    end

    context 'given valid tweet' do
      scenario 'should be successful' do
        fill_in 'Tweet', with: 'Test tweet'
        click_button 'Post Tweet'
        expect(page).to have_content('Test tweet')
      end
    end

    context 'given tweet exceeded 140 characters' do
      scenario 'should fail' do
        input = 'Test tweet'
        20.times do
          input << 'Test tweet'
        end
        fill_in 'Tweet', with: input
        click_button 'Post Tweet'
        expect(page).to have_content('140 characters is the maximum allowed')
      end
    end
  end

  context 'delete tweet' do
    let!(:tweet) { Tweet.create(text: 'Test tweet') }
    scenario 'delete tweet' do
      visit new_tweet_path
      click_link 'delete'
      expect(page).to have_content('Tweet was successfully deleted.')
    end
  end
end
